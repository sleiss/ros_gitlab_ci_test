import argparse
import hashlib
import json


def parse_entry(entry):
    if 'file' not in entry['location']:
        return None
    file = entry['location']['file']

    line = 0
    if 'line' in entry['location']:
        line = entry['location']['line']
        
    message = entry['text']
    rule = entry['id']

    fingerprint = (file + ':' + str(line) + ':' + rule).encode('utf-8')

    finding = {
        "description": message,
        "fingerprint": hashlib.md5(fingerprint).hexdigest(),
        "location": {
            "path": file,
            "lines": {
                "begin": line
            }
        }
    }
    return finding


parser = argparse.ArgumentParser(description='Migrate catkin lint output into json file.')
parser.add_argument('input', metavar='input', type=str, help='The input file')
parser.add_argument('output', metavar='output', type=str, help='The output file')

args = parser.parse_args()

findings = []


with open(args.input) as f:
    data = json.load(f)

findings = []

for error in data['errors']:
    parsed = parse_entry(error)
    if parsed is not None:
        findings.append(parsed)

for error in data['notices']:
    parsed = parse_entry(error)
    if parsed is not None:
        findings.append(parsed)

for error in data['warnings']:
    parsed = parse_entry(error)
    if parsed is not None:
        findings.append(parsed)

print(findings)

with open(args.output, 'w') as outfile:
    json.dump(findings, outfile)