import re
import argparse
import hashlib
import json

parser = argparse.ArgumentParser(description='Migrate roslint output into xml file.')
parser.add_argument('input', metavar='input', type=str, help='The input file')
parser.add_argument('output', metavar='output', type=str, help='The output file')
parser.add_argument('prefix', metavar='prefix', type=str, help='Prefix that should be stripped from the file path')

args = parser.parse_args()

pattern = re.compile("(?P<file>.*):(?P<line>\d+):  (?P<message>.*)  \[(?P<rule>.*)\] \[(?P<level>\d+)\]")


findings = []

prefix = args.prefix
if not prefix.endswith('/'):
    prefix = prefix + '/'

for i, line in enumerate(open(args.input)):
    for match in re.finditer(pattern, line):
        file = match.group('file')
        if file.startswith(prefix):
            file = file[len(prefix):]

        fingerprint = (file + ':' + match.group('line') + ':' + match.group('rule')).encode('utf-8')

        finding = {
            "description": match.group('message'),
            "fingerprint": hashlib.md5(fingerprint).hexdigest(),
            "location": {
                "path": file,
                "lines": {
                    "begin": match.group('line'),
                }
            }
        }
        findings.append(finding)


with open(args.output, 'w') as outfile:
    json.dump(findings, outfile)