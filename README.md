[![ROS](http://www.ros.org/wp-content/uploads/2013/10/rosorg-logo1.png)](https://gitlab.com/VictorLamoine/ros_gitlab_ci)

This is a test example package for [ROS GitLab CI](https://gitlab.com/VictorLamoine/ros_gitlab_ci); it includes:
- `catkin_make` build and tests
- `catkin tools` build and test (using artifacts)
- `catkin_lint` to verify the `package.xml` and `CMakeLists.txt` files

It does not test:
- `wstool` file
- git submodules
- Installing (eg: `catkin_make install`)
